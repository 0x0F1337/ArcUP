﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arcup
{
    public class Options
    {
        public MenuOption DefaultMenuOption { get; set; }
        public string[] AdditionalProcessesToBootUris { get; set; }
    }
}
