﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace arcup
{
    public class ArcUp
    {
        const string ARC_DOWNLOAD_URL = "https://www.deltaconnected.com/arcdps/x64/d3d9.dll";

        private readonly string gw2Path;
        private readonly string[] additionalProcessesToBoot;

        private string Gw2Folder
        {
            get
            {
                var aux = gw2Path.Split(Path.DirectorySeparatorChar)[..^1];

                return string.Join(Path.DirectorySeparatorChar, aux);
            }
        }


        public ArcUp(string gw2Path, params string[] additionalProcessesToBoot)
        {
            this.gw2Path = gw2Path;
            this.additionalProcessesToBoot = additionalProcessesToBoot;
        }


        /// <summary>
        /// Gets the date of the latest ArcDPS version uploaded
        /// </summary>
        /// <returns>Date of the latest version of ArcDps found</returns>
        public async Task<DateTime> GetLastArcDpsDate()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(ARC_DOWNLOAD_URL);

            return response.Content.Headers.LastModified.Value.Date;
        }


        /// <summary>
        /// Downloads ArcDPS
        /// </summary>
        public void DownloadArcDps()
        {
            const int CHUNK_DOWNLOAD_SIZE = 1024;

            WebRequest req = WebRequest.Create(ARC_DOWNLOAD_URL);

            string downloadedFilePath = GetFileName(ARC_DOWNLOAD_URL);

            Console.WriteLine(string.Format("Downloading from {0} please wait...", ARC_DOWNLOAD_URL));

            using BinaryReader br = new BinaryReader(req.GetResponse().GetResponseStream());
            using BinaryWriter bw = new BinaryWriter(File.Open(downloadedFilePath, FileMode.Create, FileAccess.Write));

            var chunk = br.ReadBytes(CHUNK_DOWNLOAD_SIZE);

            while (chunk.Length > 0)
            {
                bw.Write(chunk);
                chunk = br.ReadBytes(CHUNK_DOWNLOAD_SIZE);
            }

            Console.WriteLine("Download completed");
        }


        /// <summary>
        /// Gets a filename from an URL
        /// </summary>
        /// <param name="url">URL</param>
        /// <returns>Filename gotten from the URL</returns>
        private string GetFileName(string url)
        {
            const string BIN_64_FOLDER = "/bin64";

            var aux = url.Split('/');
            string filename = aux[aux.Length - 1];

            return $"{Gw2Folder}{BIN_64_FOLDER}/{filename}";
        }


        /// <summary>
        /// Starts GW2
        /// </summary>
        public void StartGW2()
        {
            Process.Start(gw2Path);

            foreach(string processPath in additionalProcessesToBoot)
                Process.Start(processPath);
        }
    }
}
