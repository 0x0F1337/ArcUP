﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arcup
{
    public class Ui
    {
        private readonly ArcUp arcUp;


        public Ui(ArcUp arcUp)
        {
            this.arcUp = arcUp;
        }


        /// <summary>
        /// Shows the startup message for the application
        /// </summary>
        /// <param name="defaultOption">Default option</param>
        public void Startup(MenuOption defaultOption)
        {
            string arcdpsDate = arcUp.GetLastArcDpsDate().Result.ToString("yyyy-MM-dd");

            string header = string.Format(
                "Welcome to ArcUp\n" +
                "Latest version of ArcDps found: {0}\n\n" +
                "Select an option to continue:\n" +
                "1. Download latest version\n" +
                "0. Exit", arcdpsDate);

            Console.WriteLine(header);

            // Waits for the user to introduce a valid value if default value was not given
            int opt = (int)defaultOption;

            while (opt == (int)MenuOption.Error)
            {
                opt = ToInt(Console.ReadLine());

                if (opt == -1)
                    Console.Error.WriteLine("Invalid value. Please try again");
            }

            StartupCallback((MenuOption)opt);
        }


        /// <summary>
        /// Callback for the startup method after the user introduces an option
        /// </summary>
        /// <param name="opt">option to be executed. Available options are defined in the "OPTIONS" top-level enum</param>
        private void StartupCallback(MenuOption opt)
        {
            switch (opt)
            {
                case MenuOption.Exit:
                    Environment.Exit(0);
                    break;

                case MenuOption.Download:
                    arcUp.DownloadArcDps();
                    arcUp.StartGW2();
                    break;
            }
        }


        /// <summary>
        /// Converts a string to int safely
        /// </summary>
        /// <param name="input">String to be converted</param>
        /// <param name="defaultValue">Default value if cannot convert string</param>
        /// <returns>Integer representation of the string</returns>
        private int ToInt(string input, int defaultValue = -1)
        {
            // If couldn't convert, return defaultValue
            if (!int.TryParse(input, out int output))
                return defaultValue;

            return output;
        }
    }
}
