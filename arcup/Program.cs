﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CommandLine;


namespace arcup
{
    class Program
    {
        static void Main(string gw2Path = "Gw2-64.exe", MenuOption @default = MenuOption.Download, string[] processes = null)
        {
            var arcUp = new ArcUp(gw2Path, processes);

            var ui = new Ui(arcUp);
            ui.Startup(@default);
        }
    }
}
